let gradoservicioPobre;
let gradoservicioBueno;
let gradoservicioExcelente;
let gradocomidaMala;
let gradocomidaDeliciosa;
let Maximo;
let reglaMaximo;
let datos = {};
function setDefaultValues()
{
	gradoservicioPobre = 0;
	gradoservicioBueno = 0;
	gradoservicioExcelente = 0;
	gradocomidaMala = 0;
	gradocomidaDeliciosa = 0;
	Maximo = 0;
	reglaMaximo = "";
	datos = {
		"conjuntos" : 
		{
			"servicio" : 
			{
					"pobre" : { "min" : 0, "max" : 25, "nucleo" : 0, "func" : "triangular"},
					"bueno" : { "min" : 25 , "max" : 75, "nucleo" : 50, "func" : "triangular"},
					"excelente" : { "min" : 75 , "max" : 100, "nucleo" : 100, "func" : "triangular"}
			},
			"comida" : 
			{
					"mala" : { "min" : 0, "max" : 50, "func" : "hd"},
					"deliciosa" : { "min" : 25, "max" : 100, "func" : "hi"}
			},
			"propina" : 
			{
					"poca" : { "min" : 0, "max" : 75, "func" : "hd"},
					"media" : { "min" : 75, "max" : 175, "nucleo" : 125, "func" : "triangular"},
					"generosa" : { "min" : 150, "max" : 250, "func" : "hi"}
			}
		},
		"reglas" :
		{
			"r1" : {"activada": false, "valor": null, "servicio" : "pobre", "comida" : "mala", "propina" : "poca"},
			"r2" : {"activada": false, "valor": null, "servicio" : "bueno", "propina" : "media"},
			"r3" : {"activada": false, "valor": null, "servicio" : "excelente", "comida" : "deliciosa", "propina" : "generosa"}
		}
	}
}

function calcularGradoPertenencia(a,b,m,func,x)
{
	switch(func)
	{
		case "triangular":
			if(x < a)
			{
				return 0;
			}
			else if (a < x && x <= m)
			{
				return (x - a) / (m - a);
			}
			else if (m < x && x < b)
			{
				return (b - x) / (b - m);
			}
			else if(x >= b)
			{
				return 0;
			}
		break;
		case "hi":
			if(x <= a)
			{
				return 0;
			}
			else if (a <= x && x <= b)
			{
				return (x - a) / (b - a);
			}
			else if(x >= b)
			{
				return 1;
			}
		break;
		case "hd":
			if(x <= a)
			{
				return 1;
			}
			else if (a <= x && x <= b)
			{
				return (x - b) / (a - b);
			}
			else if(x >= b)
			{
				return 0;
			}
		break;
	}	
}

function obtenerGradoservicio(suc = 30)
{
	let servicioPobre = datos.conjuntos.servicio.pobre;
	let servicioBueno = datos.conjuntos.servicio.bueno;
	let servicioExcelente = datos.conjuntos.servicio.excelente;
	gradoservicioPobre = calcularGradoPertenencia(servicioPobre.min,servicioPobre.max,servicioPobre.nucleo,servicioPobre.func,suc);
	gradoservicioBueno = calcularGradoPertenencia(servicioBueno.min,servicioBueno.max,servicioBueno.nucleo,servicioBueno.func,suc);
	gradoservicioExcelente = calcularGradoPertenencia(servicioExcelente.min,servicioExcelente.max,servicioExcelente.nucleo,servicioExcelente.func,suc);
	console.log("Servicio Pobre:" + gradoservicioPobre + "%")
	console.log("Servicio Bueno:" + gradoservicioBueno + "%")
	console.log("Servicio Excelente:" + gradoservicioExcelente + "%")
}

function obtenerGradocomida(suc = 80)
{
	let comidaMala = datos.conjuntos.comida.mala;
	let comidaDeliciosa = datos.conjuntos.comida.deliciosa;
	gradocomidaMala = calcularGradoPertenencia(comidaMala.min,comidaMala.max,comidaMala.nucleo,comidaMala.func,suc);
	gradocomidaDeliciosa = calcularGradoPertenencia(comidaDeliciosa.min,comidaDeliciosa.max,comidaDeliciosa.nucleo,comidaDeliciosa.func,suc);
	console.log("Comida mala:" + gradocomidaMala + "%")
	console.log("Comida deliciosa:" + gradocomidaDeliciosa + "%")
}

function recorrerReglas()
{
	for(let regla in datos.reglas)
	{
		switch(datos.reglas[regla].servicio)
		{
			case "pobre":
				switch(datos.reglas[regla].comida)
				{
					case "mala":
						datos.reglas[regla].valor = Math.max(gradoservicioPobre,gradocomidaMala);
						if(datos.reglas[regla].valor > 0)
						{
						 	datos.reglas[regla].activada = true;
						}
					break;/*
					case "media":
						 datos.reglas[regla].valor = Math.max(gradoservicioPobre,gradocomidaDeliciosa);
						 if(datos.reglas[regla].valor > 0)
						 {
						 	datos.reglas[regla].activada = true;
						 }
					break;*/
					case "deliciosa":
						datos.reglas[regla].valor = Math.max(gradoservicioPobre,gradocomidaDeliciosa);
						if(datos.reglas[regla].valor > 0)
						{
						 	datos.reglas[regla].activada = true;
						}
					break;
				}
			break;
			case "bueno":
				switch(datos.reglas[regla].comida)
				{
					case "mala":
						datos.reglas[regla].valor = Math.max(gradoservicioBueno,gradocomidaMala);
						if(datos.reglas[regla].valor > 0)
						{
							datos.reglas[regla].activada = true;
						}
					break;/*
					case "media":
							datos.reglas[regla].valor = Math.max(gradoservicioBueno,gradocomidaDeliciosa);
							if(datos.reglas[regla].valor > 0)
							{
								datos.reglas[regla].activada = true;
							}
					break;*/
					case "deliciosa":
						datos.reglas[regla].valor = Math.max(gradoservicioBueno,gradocomidaDeliciosa);
						if(datos.reglas[regla].valor > 0)
						{
								datos.reglas[regla].activada = true;
						}
					break;
				}
			break;
			case "excelente":
				
				switch(datos.reglas[regla].comida)
				{
					case "mala":
						datos.reglas[regla].valor = Math.max(gradoservicioExcelente,gradocomidaMala);
						if(datos.reglas[regla].valor > 0)
						{
							datos.reglas[regla].activada = true;
						}
					break;/*
					case "media":
							datos.reglas[regla].valor = Math.max(gradoservicioBueno,gradocomidaDeliciosa);
							if(datos.reglas[regla].valor > 0)
							{
								datos.reglas[regla].activada = true;
							}
					break;*/
					case "deliciosa":
						datos.reglas[regla].valor = Math.max(gradoservicioExcelente,gradocomidaDeliciosa);
						if(datos.reglas[regla].valor > 0)
						{
							datos.reglas[regla].activada = true;
						}
					break;
				}
			break;
		}
	}
}

function obtenerMaximo(comida,servicio)
{
	obtenerGradocomida(comida);
	obtenerGradoservicio(servicio);
	recorrerReglas();

	for(let regla in datos.reglas)
	{
		if(datos.reglas[regla].activada == true)
		{
			if(datos.reglas[regla].valor > Maximo)
			{
				Maximo = datos.reglas[regla].valor;
				reglaMaximo = regla;
			}
		}
	}
	console.log("La regla con maxima puntuacion es la regla: " + reglaMaximo + " con un valor de: " + Maximo);
}

function despejarGradoPertenencia(a,b,m,func,x)
{
	switch(func)
	{
		case "triangular":
			let r1 = (x * (m -a)) + a;
			let r2 = b -(x * (b -m));
			console.log( "El resultado de la propina es:" + (((r1 + r2) / 2)/10)+"%");
		break;
		case "hi":
			console.log( "El resultado de la propina es:" + (((x * (b - a)) + a))/10+"%");
		break;
		case "hd":
			console.log( "El resultado de la propina es:" + (((x * (a - b)) + b))/10+"%");
		break;
	}
}

function calcular(comida,servicio)
{
	setDefaultValues();
	obtenerMaximo(comida,servicio);
	let n = datos.reglas[reglaMaximo].propina;
	let j = datos.conjuntos.propina[n];
	despejarGradoPertenencia(j.min, j.max,j.nucleo,j.func,Maximo);
}